#pragma once
#include "Tablero.h"
#include <iostream>
#include <queue>
#include <stdlib.h>
#include <list>

using namespace std;
class programa
{
private:
	queue<Tablero> cola;
	Tablero tableroVacio;
	list<pair<int, int>> reglasAplicables;
	int cantidadTableroCreados;
	bool seSoluciono;
public:
	programa();
	~programa();
	Tablero BFS();
	bool getSeSoluciono();
	int getCantidadTablerosVisitados();
	void generarReglasAplicables(Tablero tablero);
	pair<int, int> seleccionarReglaDeAplicacion();
	Tablero aplicarReglaPosicionarReina(Tablero actual,int i, int j);
};

