#include "programa.h"
#include <chrono>
using namespace std::chrono;

int main()
{ 
	programa programaReinas;
	Tablero solucion;

	auto start = high_resolution_clock::now();
	solucion=programaReinas.BFS();
	auto stop = high_resolution_clock::now();

	double segundos = std::chrono::duration<double>(stop-start).count();
	double milisegundos = std::chrono::duration<double, std::milli>(stop - start).count();
	
	if (programaReinas.getSeSoluciono() == true)
	{
		solucion.mostrar();
		cout << "Se encontro la solucion en: " << endl;
		cout << milisegundos <<" milisegundos." << endl;
		cout << segundos << " segundos." << endl;
		cout << "Se visitaron:  " << endl;
		cout << programaReinas.getCantidadTablerosVisitados() << " tableros." << endl;
	}
	else
	{
		cout << "no se encontro solucion";
	}
	system("pause");
}