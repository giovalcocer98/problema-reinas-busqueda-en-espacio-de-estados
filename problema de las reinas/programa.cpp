#include "programa.h"



programa::programa()
{
	cantidadTableroCreados = 1;
	seSoluciono = false;
}


programa::~programa()
{
}


Tablero programa::BFS()
{
	Tablero tableroActual,nuevo;

	cola.push(tableroVacio);
	while( !cola.empty())
	{
		tableroActual = cola.front();
		cola.pop();
		generarReglasAplicables(tableroActual);
		while (!reglasAplicables.empty())
		{
			pair<int, int> posicionDeReglaAplicable;
			posicionDeReglaAplicable = seleccionarReglaDeAplicacion();
			nuevo = aplicarReglaPosicionarReina(tableroActual, posicionDeReglaAplicable.first, posicionDeReglaAplicable.second);
			cantidadTableroCreados++;
			if (nuevo.getNumeroDeReinas() == 4)
			{
				seSoluciono = true;
				return nuevo;
			}
			else
			{
				cola.push(nuevo);
			}
		}
	}
	return tableroActual;
}

bool programa::getSeSoluciono()
{
	return seSoluciono;
}

int programa::getCantidadTablerosVisitados()
{
	return cantidadTableroCreados;
}

void programa::generarReglasAplicables(Tablero tablero)
{
	int i;

	for (int j = 1; j <= TAM; j++)
	{
		i = tablero.getNumeroDeReinas() + 1;
		
		
		if (tablero.verificarReglaColocarReina(i, j) == true)//(1.1)
		{
			reglasAplicables.push_back(pair<int, int>(i, j));
		}

	}
}

pair<int, int> programa::seleccionarReglaDeAplicacion()
{
	pair<int, int> result;
	result = reglasAplicables.front();
	reglasAplicables.pop_front();
	return result;
}

Tablero programa::aplicarReglaPosicionarReina(Tablero actual, int i, int j)
{
	Tablero nuevo=actual;
	nuevo.posicionarReina(i, j);
	return nuevo;
}
