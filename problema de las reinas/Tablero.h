#pragma once
#include <iostream>
#define TAM 4
using namespace std;
class Tablero
{
private:
	int tablero[TAM];
	int numeroReinasActuales;

public:
	Tablero();
	~Tablero();
	void posicionarReina(int i, int j);
	int getNumeroDeReinas();
	void setNumeroDeReinas(int numeroReinas);
	void mostrar();
	bool verificarReglaColocarReina(int i, int j);

};

