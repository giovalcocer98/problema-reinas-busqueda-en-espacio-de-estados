#include "Tablero.h"



Tablero::Tablero()
{
	this->numeroReinasActuales = 0;
	for (int i = 0; i <= TAM; i++)
	{
		tablero[i] = 0;
	}

}


Tablero::~Tablero()
{
}

bool Tablero::verificarReglaColocarReina(int i, int j)
{
	bool result=true;
	for (int k = 1; k <= TAM; k++)
	{
		if (tablero[k - 1] == i || (tablero[k - 1] != 0 && j == k))
		{
			result = false;
		}
		if (tablero[k - 1] != 0 && (abs(i - tablero[k - 1]) == abs(j - k)))
		{
			result = false;
		}
	}
	return result;
}

void Tablero::posicionarReina(int i, int j)
{
	tablero[j - 1] = i;
	numeroReinasActuales++;
}

int Tablero::getNumeroDeReinas()
{
	return numeroReinasActuales;
}
void Tablero::setNumeroDeReinas(int numeroReinas)
{
	this->numeroReinasActuales = numeroReinas;
}
void Tablero::mostrar()
{
	for (int j = 0; j < TAM; j++)
	{
		cout << "|---";
	}
	cout << "|";
	cout << endl;
	for(int i = 0; i < TAM; i++)
	{
		cout << "| ";
		for (int j = 0; j < TAM; j++)
		{
			if (tablero[j] == i+1)
			{
				cout << 1;
			}
			else
			{
				cout << "o";
			}
			cout << " | ";
		}
		cout << endl;
		for (int j = 0; j < TAM; j++)
		{
			cout << "|---";
		}
		cout << "|";
		cout << endl;
	}
	cout << endl;
	cout << endl;
	cout << endl;
	for (int i = 1; i <= TAM; i++)
	{
		if (i != TAM)
		{
			cout << tablero[i - 1] << " - ";
		}
		else
		{
			cout << tablero[i - 1];
		}
		
	}
	cout << endl;
	
	
}